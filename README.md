# intuitive

A node boiler plate for creating icon packs from a set of svgs.
Just place your icons in the directory src/icons.

Webfont can only convert mono colored svgs. All icons in the pack
have been modified to have a width or length of 2000px. All font
files will be outputed in the dir build/fonts/intuitive/

## NPM Commands:

	// Creates the icon pack.
	npm run build
	// Places the icon pack in an html page and serves it on
	// http://localhost:3000/intuitive/test
	npm run test

## Icon Classes:

All icons should be normalized by the int class.
I'm still in the process of placing credits for my icons.

1. int
2. int-asterisk-1 : from font-awesome
3. int-bootstrap-1 :
4. int-briefcase-1 : from font-awesome
5. int-certificate-1 : from font-awesome
6. int-css3-1 : from font-awesome
7. int-down-arrow-1 : from font-awesome
8. int-envelope-1 : from font-awesome
9. int-git-1 : from font-awesome
10. int-github-1 : from font-awesome
11. int-github-2 : from font-awesome
12. int-gitlab-1 : from font-awesome
13. int-home-1 : from font-awesome
14. int-html5-1 : from font-awesome
15. int-jquery-1 : from font-awesome
16. int-js-1 : from font-awesome
17. int-menu-1 : from font-awesome
18. int-mongodb-1 :
19. int-nodejs-1 :
20. int-npm-1 : from font-awesome
21. int-page-1 :
22. int-phone-1 : from font-awesome
23. int-pugjs-1 : from pugjs
24. int-reactjs-1 : from reactjs
25. int-rocket-1 : from font-awesome
26. int-sass-1 : from sass
27. int-star-1 : from 
28. int-terminal-1 : from 
29. int-vim-1 : from
29. int-webpack-1 : from webpack
30. int-world-1 : from font-awesome
