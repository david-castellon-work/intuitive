import express			from 'express'			;
import path			from 'path'			;

var app = express ( )	;

app . set ( 'view engine' , 'pug' ) ;
app . set ( 'views' , path . resolve ( __dirname , 'views' , ) ) ; 

app . use ( '/public' , express . static ( path . resolve ( __dirname ) ) ) ;

app . use ( express . urlencoded ( { extended : false } ) ) ;

// routes
app . get ( '/test' , ( req , res ) => {
	res . render ( 'intuitive' , { path_prefix : process . argv [ 2 ] } ) ;
} ) ;

app . get ( '/' , test_redirect ) ;
app . get ( '/*' , test_redirect ) ;

export default app ;

// functions
function test_redirect ( req , res ) {
	res . redirect ( '/intuitive/test' ) ;
}
