import fs from 'fs' ;
import path from 'path' ;
import webfont from 'webfont' ;
import sass from 'node-sass' ;

var font_name = 'intuitive' ;
var font_path = path . join ( __dirname , 'fonts' ) ;
var int_font_path = path . join ( font_path  , font_name ) ;

function popDir ( dir ) {
	var dir_array = dir . split ( '/' ) ;
	dir_array . pop ( ) ;
	return ( dir_array . join ( '/' ) ) ;
}

webfont ( {
	files : path . join ( __dirname , 'icons' , '*.svg' ) ,
	fontName : font_name ,

	template : path . join ( font_path , font_name ) + '.scss.njk' ,

	templateFontName : font_name ,
	templateFontPath : '/intuitive/public/fonts' ,
	templateClassName : 'int' ,

} ) . then ( result => {


	fs . writeFile ( int_font_path + '.svg' , result . svg , err => {
		if ( err ) { console . log ( err ) ; } } ) ;
	fs . writeFile ( int_font_path + '.eot' , result . eot , err => {
		if ( err ) { console . log ( err ) ; } } ) ;
	fs . writeFile ( int_font_path + '.ttf' , result . ttf , err => {
		if ( err ) { console . log ( err ) ; } } ) ;
	fs . writeFile ( int_font_path + '.woff' , result . woff , err => {
		if ( err ) { console . log ( err ) ; } } ) ;
	fs . writeFile ( int_font_path + '.woff2' , result . woff2 , err => {
		if ( err ) { console . log ( err ) ; } } ) ;
	fs . writeFile ( int_font_path + '.scss' , result . template , err => {
		sass . render ( {
			file : int_font_path + '.scss' ,
			indentType : 'tab' ,
			outFile : int_font_path + '.css'
		} , ( err , result ) => {

			fs . writeFile ( int_font_path + '.css' , result . css , err => {
				if ( err ) { console . log ( err ) ; }
			} ) ;

			return result ;
		 } ) ;
		return err ;
	} ) ;

} ) ;

