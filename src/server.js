import express from 'express' ;
import app from './app' ;

var server = express ( ) ;
const port = process . env . PORT || '3000' ;

server . use ( '/intuitive' , app ) ;
server . get ( '/' , test_redirect ) ;
server . get ( '/*' , test_redirect ) ;
server . listen ( port ) ;

// functions
function test_redirect ( req , res ) {
	res . redirect ( '/intuitive' ) ;
}
