const path = require ( 'path' ) ;

const	DEV_MODE	= ( process . env . NODE_ENV !== 'production' ) ,
	SRC_FILE	= path . resolve ( 'src' ) ,
	BUILD_FILE	= path . resolve ( 'build' ) ;

const	gulp		= require ( 'gulp' ) ,
	newer		= require ( 'gulp-newer' ) ,
	noop		= require ( 'gulp-noop' ) ,
	sourcemaps	= ( DEV_MODE ? require ( 'gulp-sourcemaps' ) : null ) ,

	babel		= require ( 'gulp-babel' ) ,
	terser		= require ( 'gulp-terser' ) ,
	stripdebug	= ( DEV_MODE ? null : require ( 'gulp-strip-debug' ) ) ,

	sass		= require ( 'gulp-sass' ) ( require('node-sass' ) ),
	postcss		= require ( 'gulp-postcss' ) ,
	cssnano		= require ( 'cssnano' ) ;

gulp . task ( 'views' , views ) ;
gulp . task ( 'icons' , icons ) ;
gulp . task ( 'icon_style' , icon_style ) ;
gulp . task ( 'styles' , styles ) ;
gulp . task ( 'scripts' , scripts ) ;
gulp . task ( 'default' , gulp . series ( 'views' , 'icons' , 'icon_style' , 'styles' , 'scripts' ) ) ;

function views ( ) {
	return gulp . src ( 'src/views/*' )
		. pipe ( gulp . dest ( 'build/views' ) ) ;
}
function icons ( ) {
	const icon_dir = path . join ( BUILD_FILE , 'icons' ) ;
	return gulp . src ( 'src/icons/*' )
		. pipe ( newer ( icon_dir ) )
		. pipe ( gulp . dest ( icon_dir ) ) ;
}
function icon_style ( ) {
	const icon_style_dir = path . join ( BUILD_FILE , 'fonts' ) ;
	return gulp . src  ( 'src/fonts/intuitive.scss.njk' )
		. pipe ( newer ( icon_style_dir ) )
		. pipe ( gulp . dest ( icon_style_dir ) ) ;
}
function styles ( ) {
	return gulp . src  ( 'node_modules/dark/src/dark.scss' )
		. pipe ( sass ( ) . on ( 'error' , sass . logError ) )
		. pipe ( gulp . dest ( 'build/styles' ) ) ;
}
function scripts ( ) {
	return gulp . src ( 'src/*.js' )
		. pipe ( newer ( BUILD_FILE ) )
		. pipe ( sourcemaps ? sourcemaps . init ( ) : noop ( ) )
		. pipe ( babel ( {
			presets : [ '@babel/preset-env' ]
		} ) )
		. pipe ( terser ( ) )
		. pipe ( stripdebug ? stripdebug ( ) : noop ( ) )
		. pipe ( sourcemaps ? sourcemaps . mapSources ( function ( sourcePath , file ) {
			return path . join ( SRC_FILE , sourcePath ) ;
		} ) : noop ( ) )
		. pipe ( sourcemaps ? sourcemaps . write ( './maps' , {
			includeContent : false
		} ) : noop ( ) )
		. pipe ( gulp . dest ( BUILD_FILE ) ) ;
}
